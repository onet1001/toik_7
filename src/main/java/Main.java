import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {
    public static void main(String... args) throws FileNotFoundException, DocumentException {

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter.getInstance(document, new FileOutputStream("result.pdf"));
        document.open();
        TitleMaker(document);
        TableMaker(document);
        document.close();

    }
    private static void TitleMaker(Document document)throws DocumentException{
        Paragraph paragraph = new Paragraph("Resume");
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setSpacingAfter(60);
        document.add(paragraph);
    }

    private static void TableMaker(Document document)throws DocumentException{
        PdfPTable table = new PdfPTable(2);
        table.addCell("First name");
        table.addCell("Dawid");
        table.addCell("Last name");
        table.addCell("Onak");
        table.addCell("Profession");
        table.addCell("Student");
        table.addCell("Education");
        table.addCell("2018-2021 Państwowa wyższa szkoła zawodowa w Taronwie");
        table.addCell("Summary");
        table.addCell("I'm student.");
        document.add(table);
    }
}
